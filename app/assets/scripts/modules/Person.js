/***********************************************************************************************
//var Person = require("./modules/Person");
import Person from "./modules/Person"; 
/* es6 syntax to import...(just like java import except the fact that the java 
 * packages are pre known to us but here the path is specified with the help of 
 * from keyword)
 */
/*
class Adult extends Person{
    letsGoOnDrive(){
        console.log("New way to make money: by RTO!");
    }
}

var john = new Adult("John Doe", "Black");
john.greet();
john.letsGoOnDrive();

var jane = new Person("Jane Doe", "Pink");
jane.greet();

***********************************************************************************************/

class Person{
    constructor(fullName, favColor){
        this.name = fullName;
        this.favColor = favColor;    
    }
    greet(){
        console.log("Hello World! My name is " + this.name + " and I love " + this.favColor + " color!");
    }
}

//module.exports = Person;

export default Person; //es6 syntax to export
/*
 * The entire code will be exported except the functions
 *
 * eg. console.log("Hello");
 * This entire statement would be directly exported
 */