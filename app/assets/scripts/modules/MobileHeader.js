import $ from 'jquery';

class MobileHeader{
    constructor(){
        this.header = $(".header");
        this.menuIcon = $(".header__menu-icon");
        this.menuContainer = $(".header__menu-container");
        this.events();
    }
    
    events(){
        this.menuIcon.click(this.toggleMobileHeader.bind(this));
        /**
         * Bind is used to change the reference of this pointer
         * Since JS is an interpreted language, it allows this type of binding
         * The this passed without binding was the element generating event
         */
    }
    
    toggleMobileHeader(){
        this.menuContainer.toggleClass("header__menu-container__is-visible");
        this.menuIcon.toggleClass("header__menu-icon__close");
        this.header.toggleClass("header__is-expanded");
//        console.log(this);
    }
}

export default MobileHeader;